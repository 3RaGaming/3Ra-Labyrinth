--Labyrinth - A Factorio PvP Scenario
--Created by zackman0010 of 3RaGaming
--This file holds the functions that involve win conditions

require "locale/utils/event"

cell_captured = cell_captured or script.generate_event_name()
game_ended = game_ended or script.generate_event_name()

--Technically a GUI function, but it's a GUI regarding win conditions.
local function update_score(team, num, den)
	for _,p in pairs(game.players) do
		p.gui.top.labyrinth_status.labyrinth_status_table.labyrinth_status_toggle_frame.labyrinth_status_toggle_table.labyrinth_victory_table["labyrinth_victory_" .. team .. "_bar"].value = (num / den)
		p.gui.top.labyrinth_status.labyrinth_status_table.labyrinth_status_toggle_frame.labyrinth_status_toggle_table.labyrinth_victory_table["labyrinth_victory_" .. team .. "_text"].caption = {"status.text", num, den}
	end
end

local function match_elapsed_time()
	local returnstring = ""
	local ticks = game.tick - global.start_tick
	if ticks < 0 then
		ticks = ticks * -1
		returnstring = "-"
	end
	local hours = math.floor(ticks / 60^3)
	local minutes = math.floor((ticks % 60^3) / 60^2)
	local seconds = math.floor((ticks % 60^2) / 60)
	if hours > 0 then returnstring = returnstring .. hours .. (hours > 1 and " hours, " or " hour, ") end
	if minutes > 0 then returnstring = returnstring .. minutes .. (minutes > 1 and " minutes," or " minute,") .. " and " end
	returnstring = returnstring .. seconds .. (seconds ~= 1 and " seconds" or " second")
	return returnstring
end

local function team_won(team)
	print("PVPROUND$end," .. global.round_number .. "," .. team .. "," .. match_elapsed_time())
	game.print({"messages.won", team})
	global.started = false
	game.remove_offline_players()
	for _,p in pairs(game.players) do
		local position = game.surfaces[1].find_non_colliding_position("player", {0,0}, 0, 1)
		local character = game.surfaces[1].create_entity{name="player", position=position, force="player"}
		p.set_controller{type=defines.controllers.ghost}
		p.teleport(position, game.surfaces[1])
		p.set_controller{type=defines.controllers.character, character=character}
		p.force = "player"
	end
	local event = {["winner"] = team}
	if global.win_condition == "Domination" then
		event["cells"] = {
			["Green"] = global.owned["Green"],
			["Blue"] = global.owned["Blue"]
		}
	elseif global.win_condition == "Elimination" then
		event["points"] = {
			["Green"] = global.points["Green"],
			["Blue"] = global.points["Blue"]
		}
	end
	script.raise_event(game_ended, event)
	game.delete_surface("Labyrinth")
	game.forces["Green"].reset()
	game.forces["Blue"].reset()
end

--Function to keep track of how many cells are currently owned by each team
Event.register(cell_captured, function(event)
	if global.win_condition ~= "Domination" then return end
	local previous_team = event.previous_team
	local team = event.team
	local total_cells = global.mazesize * global.mazesize
	local victory_cells = math.ceil(total_cells * (global.domination_limit / 100))
	if previous_team == "Green" or previous_team == "Blue" then
		global.owned[previous_team] = global.owned[previous_team] - 1
		update_score(previous_team, global.owned[previous_team], victory_cells)
	end
	if team == "Green" or team == "Blue" then
		global.owned[team] = global.owned[team] + 1
		update_score(team, global.owned[team], victory_cells)
		if global.owned[team] == victory_cells then
			team_won(team)
		end
	end
end)

local points = {
	["player"] = 5,
	["rocket-silo"] = 250,
	["gun-turret"] = 1,
	["laser-turret"] = 3,
	["flamethrower-turret"] = 5,
	["roboport"] = 50
}

--Add points to a team according to the table above
Event.register(defines.events.on_entity_died, function(event)
	if global.win_condition ~= "Elimination" then return end
	if not global.started then return end
	local entity = event.entity
	if not entity or not entity.valid then return end
	if entity.force.name ~= "Green" and entity.force.name ~= "Blue" then return end
	if not points[entity.name] then return end
	if not event.cause and not event.force then return end
	local force_dead = entity.force.name
	local force_killer = event.force.name or event.cause.force.name
	if force_dead == force_killer then return end
	global.points[force_killer] = global.points[force_killer] + points[entity.name]
	update_score(force_killer, global.points[force_killer], global.elimination_limit)
	if global.points[force_killer] >= global.elimination_limit then
		team_won(force_killer)
	end
end)

--Award points for launching a rocket
Event.register(defines.events.on_rocket_launched, function(event)
	if global.win_confition ~= "Elimination" then return end
	if not global.started then return end
	local entity = event.entity
	if not entity or not entity.valid then return end
	local force = entity.force.name
	if force ~= "Green" and force ~= "Blue" then return end
	global.points[force] = global.points[force] + 500
	game.print({"messages.rocket", force})
	update_score(force, global.points[force], global.elimination_limit)
	if global.points[force] >= global.elimination_limit then
		team_won(force)
	end
end)
