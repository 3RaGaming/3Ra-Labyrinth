--Labyrinth - A Factorio PvP Scenario
--Created by zackman0010 and AaronAuraGuardian of 3RaGaming
--This file holds the functions that involve placing radars in the Labyrinth

require "locale/utils/event"

local function findCellsInChunk(chunk)
	local chunk_x = chunk.x
	local chunk_y = chunk.y
	
	local length_tiles = (global.mazesize * global.cellsize) + global.mazesize
	
	local corners = {
		top_left = {x = chunk_x * 32, y = chunk_y * 32},
		top_right = {x = (chunk_x * 32) + 32, y = chunk_y * 32},
		bottom_left = {x = chunk_x * 32, y = (chunk_y * 32) + 32},
		bottom_right = {x = (chunk_x * 32) + 32, y = (chunk_y * 32) + 32}
	}
	
	local cells = {}
	
	for _,position in pairs(corners) do
		local x = math.ceil((position.x + math.floor(length_tiles / 2)) / (global.cellsize + 1))
		local y = math.ceil((position.y + math.floor(length_tiles / 2)) / (global.cellsize + 1))
		if x > 0 and y > 0 and x <= global.mazesize and y <= global.mazesize then
			cells[x .. "," .. y] = {x=x,y=y}
		end
	end
	
	return cells
end

local function distance(point1, point2)
	return math.sqrt((point2.y - point1.y)^2 + (point2.x - point1.x)^2)
end

local function generateRadar(targetChunk)
	local cellsInChunk = findCellsInChunk(targetChunk)
	
	local radar = nil
	local cell = nil
	
	--Check to see if any of the cells have their bottom-right corner in the chunk
	for _,icell in pairs(cellsInChunk) do
		local bottom_right = {x = global.top_left.x + (icell.x * (global.cellsize + 1)) - 2, y = global.top_left.y + (icell.y * (global.cellsize + 1)) - 2}
		if math.floor(bottom_right.x / 32) == targetChunk.x and math.floor(bottom_right.y / 32) == targetChunk.y then
			radar = game.surfaces.Labyrinth.create_entity{name="radar", force="Labyrinth", position = bottom_right}
			cell = {x = icell.x, y = icell.y}
		end
	end

	--If no bottom_right corner is in the chunk, check for bottom-left
	if not radar then
		for _,icell in pairs(cellsInChunk) do
			local bottom_left = {x = global.top_left.x + ((icell.x - 1) * (global.cellsize + 1)) + 2, y = global.top_left.y + (icell.y * (global.cellsize + 1)) - 2}
			if math.floor(bottom_left.x / 32) == targetChunk.x and math.floor(bottom_left.y / 32) == targetChunk.y then
				radar = game.surfaces.Labyrinth.create_entity{name="radar", force="Labyrinth", position = bottom_left}
				cell = {x = icell.x, y = icell.y}
			end
		end
	end

	--If no bottom_left corner is in the chunk, check for top_right
	if not radar then
		for _,icell in pairs(cellsInChunk) do
			local top_right = {x = global.top_left.x + (icell.x * (global.cellsize + 1)) - 2, y = global.top_left.y + ((icell.y - 1) * (global.cellsize + 1)) + 2}
			if math.floor(top_right.x / 32) == targetChunk.x and math.floor(top_right.y / 32) == targetChunk.y then
				radar = game.surfaces.Labyrinth.create_entity{name="radar", force="Labyrinth", position = top_right}
				cell = {x = icell.x, y = icell.y}
			end
		end
	end

	--If no top_right corner, check for top_left
	if not radar then
		for _,icell in pairs(cellsInChunk) do
			local top_left = {x = global.top_left.x + ((icell.x - 1) * (global.cellsize + 1)) + 2, y = global.top_left.y + ((icell.y - 1) * (global.cellsize + 1)) + 2}
			if math.floor(top_left.x / 32) == targetChunk.x and math.floor(top_left.y / 32) == targetChunk.y then
				radar = game.surfaces.Labyrinth.create_entity{name="radar", force="Labyrinth", position = top_left}
				cell = {x = icell.x, y = icell.y}
			end
		end
	end

	--If the chunk is not touching the corner of any cell, then place the radar below the center roboport
	if not radar then
		for _,icell in pairs(cellsInChunk) do
			local top_left = {x = global.top_left.x + ((icell.x - 1) * (global.cellsize + 1)), y = global.top_left.y + ((icell.y - 1) * (global.cellsize + 1))}
			local bottom_right = {x = global.top_left.x + (icell.x * (global.cellsize + 1)), y = global.top_left.y + (icell.y * (global.cellsize + 1))}
			local center = {x = ((top_left.x + bottom_right.x) / 2), y = ((top_left.y + bottom_right.y) / 2) + 3}
			if math.floor(center.x / 32) == targetChunk.x and math.floor(center.y / 32) == targetChunk.y then
				radar = game.surfaces.Labyrinth.create_entity{name="radar", force="Labyrinth", position = center}
				cell = {x = icell.x, y = icell.y}
			end
		end
	end

	--If the radar is not able to be placed below the roboport, then place the radar to the left of the center roboport
	if not radar then
		for _,icell in pairs(cellsInChunk) do
			local top_left = {x = global.top_left.x + ((icell.x - 1) * (global.cellsize + 1)), y = global.top_left.y + ((icell.y - 1) * (global.cellsize + 1))}
			local bottom_right = {x = global.top_left.x + (icell.x * (global.cellsize + 1)), y = global.top_left.y + (icell.y * (global.cellsize + 1))}
			local center = {x = ((top_left.x + bottom_right.x) / 2) - 3, y = ((top_left.y + bottom_right.y) / 2)}
			if math.floor(center.x / 32) == targetChunk.x and math.floor(center.y / 32) == targetChunk.y then
				radar = game.surfaces.Labyrinth.create_entity{name="radar", force="Labyrinth", position = center}
				cell = {x = icell.x, y = icell.y}
			end
		end
	end

	--If the radar is not able to be placed to the left of the roboport, then place the radar above the center roboport
	if not radar then
		for _,icell in pairs(cellsInChunk) do
			local top_left = {x = global.top_left.x + ((icell.x - 1) * (global.cellsize + 1)), y = global.top_left.y + ((icell.y - 1) * (global.cellsize + 1))}
			local bottom_right = {x = global.top_left.x + (icell.x * (global.cellsize + 1)), y = global.top_left.y + (icell.y * (global.cellsize + 1))}
			local center = {x = ((top_left.x + bottom_right.x) / 2), y = ((top_left.y + bottom_right.y) / 2) - 3}
			if math.floor(center.x / 32) == targetChunk.x and math.floor(center.y / 32) == targetChunk.y then
				radar = game.surfaces.Labyrinth.create_entity{name="radar", force="Labyrinth", position = center}
				cell = {x = icell.x, y = icell.y}
			end
		end
	end

	--If the chunk is not touching the center or any corner, then place it as close to a wall as possible
	--This is almost guaranteed (exception noted below) to work if all of the above have failed due to the max cellsize being 49
	if not radar then
		local length_tiles = (global.mazesize * global.cellsize) + global.mazesize
		local chunk_top_left = {x = targetChunk.x * 32, y = targetChunk.y * 32}
		local chunk_bottom_right = {x = (targetChunk.x * 32) + 32, y = (targetChunk.y * 32) + 32}
		local max_distance_from_center = 0
		local position = {x=0,y=0}
		for _,wall in pairs(game.surfaces.Labyrinth.find_entities_filtered{name="stone-wall", force="Labyrinth", area={chunk_top_left, chunk_bottom_right}}) do
			local icell = {x = math.ceil((wall.position.x + math.floor(length_tiles / 2)) / (global.cellsize + 1)), y = math.ceil((wall.position.y + math.floor(length_tiles / 2)) / (global.cellsize + 1))}
			if icell.x == 0 then icell.x = 1 end
			if icell.y == 0 then icell.y = 1 end
			local top_left = {x = global.top_left.x + ((icell.x - 1) * (global.cellsize + 1)), y = global.top_left.y + ((icell.y - 1) * (global.cellsize + 1))}
			local bottom_right = {x = global.top_left.x + (icell.x * (global.cellsize + 1)), y = global.top_left.y + (icell.y * (global.cellsize + 1))}
			local center = {x = ((top_left.x + bottom_right.x) / 2) - 3, y = ((top_left.y + bottom_right.y) / 2)}
			local distance_from_center = distance(wall.position, center)
			if distance_from_center > max_distance_from_center then
				max_distance_from_center = distance_from_center
				position = wall.position
			end
		end

		--Check if it can be placed above the wall
		if math.floor(position.x / 32) == targetChunk.x and math.floor((position.y - 2) / 32) == targetChunk.y and game.surfaces.Labyrinth.can_place_entity{name="radar", force="Labyrinth", position = {x = position.x, y = position.y - 2}} then
			local x = math.ceil((position.x + math.floor(length_tiles / 2)) / (global.cellsize + 1))
			local y = math.ceil(((position.y - 2) + math.floor(length_tiles / 2)) / (global.cellsize + 1))
			if x > 0 and y > 0 and x <= global.mazesize and y <= global.mazesize then
				radar = game.surfaces.Labyrinth.create_entity{name="radar", force="Labyrinth", position = {x = position.x, y = position.y - 2}}
				cell = {x=x,y=y}
			end
		end

		--Check if it can be placed below the wall
		if not radar and math.floor(position.x / 32) == targetChunk.x and math.floor((position.y + 2) / 32) == targetChunk.y and game.surfaces.Labyrinth.can_place_entity{name="radar", force="Labyrinth", position = {x = position.x, y = position.y + 2}} then
			local x = math.ceil((position.x + math.floor(length_tiles / 2)) / (global.cellsize + 1))
			local y = math.ceil(((position.y + 2) + math.floor(length_tiles / 2)) / (global.cellsize + 1))
			if x > 0 and y > 0 and x <= global.mazesize and y <= global.mazesize then
				radar = game.surfaces.Labyrinth.create_entity{name="radar", force="Labyrinth", position = {x = position.x, y = position.y + 2}}
				cell = {x=x,y=y}
			end
		end

		--Check if it can be placed to the right of the wall
		if not radar and math.floor((position.x + 2) / 32) == targetChunk.x and math.floor((position.y ) / 32) == targetChunk.y and game.surfaces.Labyrinth.can_place_entity{name="radar", force="Labyrinth", position = {x = position.x + 2, y = position.y}} then
			local x = math.ceil(((position.x + 2) + math.floor(length_tiles / 2)) / (global.cellsize + 1))
			local y = math.ceil((position.y + math.floor(length_tiles / 2)) / (global.cellsize + 1))
			if x > 0 and y > 0 and x <= global.mazesize and y <= global.mazesize then
				radar = game.surfaces.Labyrinth.create_entity{name="radar", force="Labyrinth", position = {x = position.x + 2, y = position.y}}
				cell = {x=x,y=y}
			end
		end

		--Check if it can be placed to the left of the wall
		if not radar and math.floor((position.x - 2) / 32) == targetChunk.x and math.floor((position.y ) / 32) == targetChunk.y and game.surfaces.Labyrinth.can_place_entity{name="radar", force="Labyrinth", position = {x = position.x - 2, y = position.y}} then
			local x = math.ceil(((position.x - 2) + math.floor(length_tiles / 2)) / (global.cellsize + 1))
			local y = math.ceil((position.y + math.floor(length_tiles / 2)) / (global.cellsize + 1))
			if x > 0 and y > 0 and x <= global.mazesize and y <= global.mazesize then
				radar = game.surfaces.Labyrinth.create_entity{name="radar", force="Labyrinth", position = {x = position.x - 2, y = position.y}}
				cell = {x=x,y=y}
			end
		end
	end

	--Perform the rest of the bonus logic
	--It is still possible for radar to be nil here, typically if the edge of the Labyrinth has one or no open tiles in a chunk (Tile and Wall or only Wall)
	--If that happens, then those one or two tiles will be without radar coverage
	if radar then
		radar.minable = false
		radar.destructible = false
		table.insert(global.refill_energy, {entity = radar, cell = {x=cell.x,y=cell.y}})
		global.radars[cell.x .. "," .. cell.y] = radar
	end
end

--luacheck: ignore add_radars
function add_radars()
	local labyrinthSize = (global.mazesize * (global.cellsize + 1)) + 1
	local labyrinthChunks = labyrinthSize / 32
	local radarSize = math.ceil(labyrinthChunks / 4)

	generateRadar({x=0,y=0})
	radarSize = radarSize - 1
	local outermostChunk = 0

	while radarSize > 0 do
		outermostChunk = outermostChunk + 4
		local currentChunk = {x = outermostChunk, y = outermostChunk}
		while currentChunk.x > (outermostChunk * -1) do --Places radars in chunks moving from bottom-right corner to bottom-left corner
			generateRadar(currentChunk)
			currentChunk.x = currentChunk.x - 4
		end
		while currentChunk.y > (outermostChunk * -1) do --Places radars in chunks moving from bottom-left corner to top-left corner
			generateRadar(currentChunk)
			currentChunk.y = currentChunk.y - 4
		end
		while currentChunk.x < outermostChunk do --Places radars in chunks moving from upper-left corner to top-right corner
			generateRadar(currentChunk)
			currentChunk.x = currentChunk.x + 4
		end
		while currentChunk.y < outermostChunk do --Places radars in chunks moving from upper-right corner to bottom-right corner
			generateRadar(currentChunk)
			currentChunk.y = currentChunk.y + 4
		end
		radarSize = radarSize - 2
	end
end
