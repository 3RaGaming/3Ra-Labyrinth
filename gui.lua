--Labyrinth - A Factorio PvP Scenario
--Created by zackman0010 of 3RaGaming
--This file holds the functions that involve the GUI

require "locale/utils/event"

cell_captured = cell_captured or script.generate_event_name()
game_ended = game_ended or script.generate_event_name()
game_started = game_started or script.generate_event_name()

--luacheck: globals colors

local function create_status_gui(p)
	--Timestamp calculation
	local delay_in_ticks = global.delay * 60 * 60
	local modulo = (game.tick - global.start_tick) % delay_in_ticks
	local time_to_change = delay_in_ticks - modulo
	if time_to_change == 0 then time_to_change = delay_in_ticks end
	local minutes = math.floor((time_to_change % 60^3) / 60^2)
	local seconds = math.floor((time_to_change % 60^2) / 60)
	if seconds < 10 then seconds = "0" .. seconds end
	local timestamp = minutes .. ":" .. seconds

	local flow = p.gui.top.add{type="flow", name="labyrinth_status"}.add{type="table", name="labyrinth_status_table", colspan=1}
	flow.add{type="button", name="labyrinth_status_open", caption={"buttons.status"}}

	--[[
	local sprite = flow.add{type="sprite-button", name="labyrinth_status_open", sprite="utility/clock"}
	sprite.style.minimal_height = 32
	sprite.style.minimal_width = 32
	sprite.style.maximal_height = 32
	sprite.style.maximal_width = 32
	sprite.style.top_padding = 0
	sprite.style.bottom_padding = 0
	sprite.style.left_padding = 0
	sprite.style.right_padding = 0
	]]--

	--This table will hold the rest of the GUI
	--When it starts, it is not visible. This causes the frame to show only the button
	--When it is visible, the timestamp and the victory progress bars will be displayed
	local frame = flow.add{type="frame", name="labyrinth_status_toggle_frame"}
	local tbl = frame.add{type="table", name="labyrinth_status_toggle_table", colspan=1}
	frame.style.visible = false

	tbl.add{type="button", name="labyrinth_status_close", caption={"buttons.close"}}

	tbl.add{type="label", name="labyrinth_timer", caption={"status.timer", timestamp}}

	local victory = tbl.add{type="table", name="labyrinth_victory_table", colspan=3}

	local green_num
	local green_den
	local blue_num
	local blue_den
	if global.win_condition == "Domination" then
		green_num = global.owned["Green"]
		green_den = math.ceil((global.mazesize * global.mazesize) * (global.domination_limit / 100))
		blue_num = global.owned["Blue"]
		blue_den = math.ceil((global.mazesize * global.mazesize) * (global.domination_limit / 100))
	elseif global.win_condition == "Elimination" then
		green_num = global.points["Green"]
		green_den = global.elimination_limit
		blue_num = global.points["Blue"]
		blue_den = global.elimination_limit
	end

	victory.add{type="label", name="labyrinth_victory_green", caption={"status.victory", {"teams.green"}}}
	victory.add{type="progressbar", name="labyrinth_victory_Green_bar", size=100}.style.smooth_color = colors.green
	victory.add{type="label", name="labyrinth_victory_Green_text", caption={"status.text", green_num, green_den}}.style.font_color = colors.green
	victory.add{type="label", name="labyrinth_victory_blue", caption={"status.victory", {"teams.blue"}}}
	victory.add{type="progressbar", name="labyrinth_victory_Blue_bar", size=100}.style.smooth_color = colors.blue
	victory.add{type="label", name="labyrinth_victory_Blue_text", caption={"status.text", blue_num, blue_den}}.style.font_color = colors.blue
end

local function create_setup_gui(player)
	local frame = player.gui.center.add{type="frame", name="labyrinth_config", direction="vertical", caption={"setup.config"}}
	local ctable = frame.add{type="table", name="labyrinth_table", colspan=2}
	ctable.add{type="label", name="labyrinth_phases_label", caption={"setup.phases"}}
	ctable.add({type="textfield", name="labyrinth_phases"}).text = global.total_phases
	ctable.add{type="label", name="labyrinth_mazesize_label", caption={"setup.maze"}}
	ctable.add({type="textfield", name="labyrinth_mazesize"}).text = global.mazesize
	ctable.add{type="label", name="labyrinth_cellsize_label", caption={"setup.cells"}}
	ctable.add({type="textfield", name="labyrinth_cellsize"}).text = global.cellsize
	ctable.add{type="label", name="labyrinth_delay_label", caption={"setup.delay"}}
	ctable.add({type="textfield", name="labyrinth_delay"}).text = global.delay
	ctable.add{type="label", name="labyrinth_capture_label", caption={"setup.capture"}}
	ctable.add({type="textfield", name="labyrinth_capture"}).text = global.capture_time
	ctable.add{type="label", name="labyrinth_biters_label", caption={"setup.biters"}}
	ctable.add{type="checkbox", name="labyrinth_biters", state=global.biters}
	ctable.add{type="label", name="labyrinth_win_label", caption={"setup.win"}}
	ctable.add{type="drop-down", name="labyrinth_win", items={{"conditions.Domination"}, {"conditions.Elimination"}}, selected_index=1}
	ctable.add{type="label", name="labyrinth_domination_label", caption={"setup.dom"}}
	ctable.add({type="textfield", name="labyrinth_domination"}).text = global.domination_limit
	ctable.add{type="label", name="labyrinth_elimination_label", caption={"setup.elim"}}.style.visible = false
	local elim = ctable.add{type="textfield", name="labyrinth_elimination"}
	elim.text = global.elimination_limit
	elim.style.visible = false
	frame.add{type="button", name="labyrinth_complete", caption={"buttons.begin"}}
end

Event.register(defines.events.on_gui_selection_state_changed, function(event)
	if not event.element or not event.element.valid then return end
	local dropdown = event.element
	if dropdown.selected_index == 1 then
		dropdown.parent.labyrinth_domination_label.style.visible = true
		dropdown.parent.labyrinth_domination.style.visible = true
		dropdown.parent.labyrinth_elimination_label.style.visible = false
		dropdown.parent.labyrinth_elimination.style.visible = false
	elseif dropdown.selected_index == 2 then
		dropdown.parent.labyrinth_domination_label.style.visible = false
		dropdown.parent.labyrinth_domination.style.visible = false
		dropdown.parent.labyrinth_elimination_label.style.visible = true
		dropdown.parent.labyrinth_elimination.style.visible = true
	end
end)

local assembly_bonuses = {
	["uranium-rounds-magazine"] = true,
	["grenade"] = true,
	["low-density-structure"] = true,
	["rocket-fuel"] = true,
	["rocket-control-unit"] = true,
	["flying-robot-frame"] = true,
	["speed-module-3"] = true,
	["effectivity-module-3"] = true,
	["productivity-module-3"] = true,
	["science-pack-1"] = true,
	["science-pack-2"] = true,
	["science-pack-3"] = true,
	["military-science-pack"] = true,
	["production-science-pack"] = true,
	["high-tech-science-pack"] = true,
	["solar-panel"] = true,
	["accumulator"] = true,
	["lab"] = true
}

local mine_bonuses = {
	["uranium-ore"] = true,
	["iron-ore"] = true,
	["copper-ore"] = true,
	["coal"] = true,
	["stone"] = true
}

local function build_tooltip(x, y)
	local tooltip = {"tooltip.tooltip", x, y}

	--Who controls this cell?
	local control = global.cell_control[x][y]
	if control == "black" then
		table.insert(tooltip, {"tooltip.unowned"})
	elseif control == "red" then
		table.insert(tooltip, {"tooltip.owned", {"teams.biter"}})
	elseif control == "Green" then
		table.insert(tooltip, {"tooltip.owned", {"teams.green"}})
	elseif control == "Blue" then
		table.insert(tooltip, {"tooltip.owned", {"teams.blue"}})
	end

	--What bonus is located in this cell?
	local bonus = global.bonuses[x][y]
	if assembly_bonuses[bonus] then
		table.insert(tooltip, {"tooltip.bonus_am"})
	elseif mine_bonuses[bonus] then
		table.insert(tooltip, {"tooltip.bonus_mine"})
	else
		table.insert(tooltip, {"tooltip.bonus_other"})
	end
	table.insert(tooltip, {"bonuses." .. bonus})

	--Roboport present?
	if global.roboports[x .. "," .. y] then
		table.insert(tooltip, {"tooltip.roboport"})
	else
		table.insert(tooltip, "")
	end

	--Radar present?
	if global.radars[x .. "," .. y] then
		table.insert(tooltip, {"tooltip.radar"})
	else
		table.insert(tooltip, "")
	end

	return tooltip
end

local function create_map(player)
	if player.gui.center.labyrinth_map_frame then return end
	local frame = player.gui.center.add{type="frame", name="labyrinth_map_frame", caption={"cells.map"}}
	frame.style.maximal_width = 800
	frame.style.maximal_height = 900
	local wrapper = frame.add{type="table", name="labyrinth_wrapper", colspan=1} --Used to put the close button below the map
	local selections = wrapper.add{type="table", name="labyrinth_map_selection", colspan=2}
	selections.add{type="label", name="labyrinth_map_control_label", caption={"buttons.control"}}
	selections.add{type="label", name="labyrinth_map_bonuses_label", caption={"buttons.bonuses"}}
	selections.add{type="radiobutton", name="labyrinth_map_control", state=true}
	selections.add{type="radiobutton", name="labyrinth_map_bonuses", state=false}
	local map = wrapper.add{type="table", name="labyrinth_map", colspan=global.mazesize}
	map.style.horizontal_spacing = 5
	map.style.vertical_spacing = 5
	map.style.cell_spacing = 0
	for y=1,global.mazesize do
		for x=1,global.mazesize do
			--Sprite names are case sensitive, so the capitalized force names need to be lower-cased
			local color = global.cell_control[x][y]
			if color == "Green" then color = "green"
			elseif color == "Blue" then color = "blue"
			end
			local sprite = map.add{type="sprite-button", name="labyrinth_map_" .. x .. "_" .. y, sprite="virtual-signal/signal-" .. color}
			sprite.style.minimal_width = 750 / (global.mazesize + 5)
			sprite.style.maximal_width = 750 / (global.mazesize + 5)
			sprite.style.minimal_height = 750 / (global.mazesize + 5)
			sprite.style.maximal_height = 750 / (global.mazesize + 5)
			sprite.style.left_padding = 0
			sprite.style.right_padding = 0
			sprite.style.top_padding = 0
			sprite.style.bottom_padding = 0
			sprite.tooltip = build_tooltip(x,y)
		end
	end
	wrapper.add{type="button", name="close_labyrinth_map", caption={"buttons.closemap"}}
end

local function redraw_map(player, map_type)
	local map = player.gui.center.labyrinth_map_frame.labyrinth_wrapper.labyrinth_map
	for y=1,global.mazesize do
		for x=1,global.mazesize do
			local sprite = ""
			if map_type == "control" then
				local color = global.cell_control[x][y]
				if color == "Green" then color = "green"
				elseif color == "Blue" then color = "blue"
				end
				sprite = "virtual-signal/signal-" .. color
			elseif map_type == "bonuses" then
				if global.bonuses[x][y] == "market" then sprite = "entity/" .. global.bonuses[x][y]
				else sprite = "item/" .. global.bonuses[x][y]
				end
			end
			map["labyrinth_map_" .. x .. "_" .. y].sprite = sprite
			map["labyrinth_map_" .. x .. "_" .. y].tooltip = build_tooltip(x,y)

		end
	end
end

local function create_debug_menu(player)
	--As this function is only run for zackman0010, it is not localized
	if player.gui.center.labyrinth_debug_menu then return end
	local frame = player.gui.center.add{type="frame", name="labyrinth_debug_menu", caption="DEBUG MENU"}.add{type="table", name="labyrinth_debug_table", colspan=2}
	frame.add{type="button", name="debug_begin_round", caption="Begin Round (Current Settings)"}
	frame.add{type="button", name="debug_open_setup", caption="Open Setup Menu"}
	frame.add{type="button", name="debug_open_join", caption="Open Join Menu"}
	frame.add{type="button", name="debug_teleport", caption="Teleport to Labyrinth"}
	frame.add{type="button", name="debug_green", caption="Green"}
	frame.add{type="button", name="debug_blue", caption="Blue"}
	frame.add{type="button", name="debug_labyrinth", caption="Labyrinth"}
	frame.add{type="button", name="debug_research", caption="Research All Techs"}
	frame.add{type="button", name="debug_open_gates", caption="Open All Gates"}
	frame.add{type="button", name="debug_resume_gates", caption="Return Gates to Scenario Control"}
	frame.add{type="button", name="debug_activate_bonuses", caption="Activate All Bonuses"}
	frame.add{type="button", name="debug_deactivate_bonuses", caption="Deactivate All Bonuses"}
	frame.add{type="button", name="debug_resume_bonuses", caption="Restore Bonuses to Scenario Control"}
	frame.add{type="button", name="close_labyrinth_debug", caption="Close Debug"}
end

Event.register(defines.events.on_player_created, function(event)
	local player = game.players[event.player_index]
	if player.name == "zackman0010" then
		player.gui.top.add{type="button", name="labyrinth_debug_menu", caption="DEBUG"}
		if global.started then
			player.gui.top.add{type="button", name="open_labyrinth_map", caption="Map"}
			create_status_gui(player)
		end
	elseif player.admin and not global.generating and not global.started then
		create_setup_gui(player)
		player.gui.center.add{type="flow", name="labyrinth_welcome"}.add{type="table", name="labyrinth_welcome_table", colspan=1}.add{type="button", name="labyrinth_welcome_close", caption={"buttons.join", "Team"}}.style.visible = false
	else
		local frame = player.gui.center.add{type="frame", name="labyrinth_welcome", caption={"welcome.welcome"}}
		frame.style.maximal_width = 750
		local tbl = frame.add{type="table", name="labyrinth_welcome_table", colspan=1}
		for i=1,14 do
			tbl.add{type="label", name="labyrinth_welcome_" .. i, caption={"welcome.welcome" .. i}, single_line = false}.style.maximal_width = 700
		end
		tbl.add{type="button", name="labyrinth_welcome_close", caption={"buttons.join", "Team"}}.style.visible = false
		if global.started then
			tbl.labyrinth_welcome_close.style.visible = true
			player.gui.top.add{type="button", name="open_labyrinth_map", caption={"buttons.map"}}
			create_status_gui(player)
		end
	end
end)

--GUI management when the game ends
Event.register(game_ended, function(event)
	--Variable declarations
	local winner = event.winner
	local cells
	local total_cells
	local points
	if global.win_condition == "Domination" then
		cells = event.cells
		total_cells = global.mazesize * global.mazesize
	elseif global.win_condition == "Elimination" then
		points = event.points
	end

	--Used to detect if an admin is online
	local admin = false

	for _,p in pairs(game.players) do
		p.gui.top.labyrinth_status.destroy()
		p.gui.top.open_labyrinth_map.destroy()
		if p.gui.center.labyrinth_map_frame then p.gui.center.labyrinth_map_frame.destroy() end

		if p.admin then
			--Create the setup GUI if an admin is online
			create_setup_gui(p)
			p.gui.center.add{type="flow", name="labyrinth_welcome"}.add{type="table", name="labyrinth_welcome_table", colspan=1}.add{type="button", name="labyrinth_welcome_close", caption={"buttons.join", "Team"}}.style.visible = false
			admin = true
		else
			--For non-admins, show an end-game report
			local frame = p.gui.center.add{type="frame", name="labyrinth_welcome", caption={"end.complete", winner}}.add{type="table", name="labyrinth_welcome_table", colspan=1}
			frame.add{type="label", name="labyrinth_end_message_1", caption={"end.end"}}

			if global.win_condition == "Domination" then
				--Show number of cells controlled by each team
				if winner == "Green" then
					frame.add{type="label", name="labyrinth_dom_green_win", caption={"end.dom_win", {"teams.green"}, cells["Green"], total_cells}}
					frame.add{type="label", name="labyrinth_dom_blue_lose", caption={"end.dom_lose", {"teams.blue"}, cells["Blue"]}}
				elseif winner == "Blue" then
					frame.add{type="label", name="labyrinth_dom_blue_win", caption={"end.dom_win", {"teams.blue"}, cells["Blue"], total_cells}}
					frame.add{type="label", name="labyrinth_dom_green_lose", caption={"end.dom_lose", {"teams.green"}, cells["Green"]}}
				end
			elseif global.win_condition == "Elimination" then
				--Show number of points each team obtained
				if winner == "Green" then
					frame.add{type="label", name="labyrinth_eli_green_win", caption={"end.elim_win", {"teams.green"}, points["Green"]}}
					frame.add{type="label", name="labyrinth_eli_blue_lose", caption={"end.elim_lose", {"teams.blue"}, points["Blue"]}}
				elseif winner == "blue" then
					frame.add{type="label", name="labyrinth_eli_blue_win", caption={"end.elim_win", {"teams.blue"}, points["Blue"]}}
					frame.add{type="label", name="labyrinth_eli_green_lose", caption={"end.elim_lose", {"teams.green"}, points["Green"]}}
				end
			end

			frame.add{type="label", name="labyrinth_end_message_2", caption={"end.next"}}
			frame.add{type="button", name="labyrinth_welcome_close", caption={"buttons.join", "Team"}}.style.visible = false
		end
	end

	if not admin then
		--luacheck: globals begin_round
		begin_round()
	end
end)

local function create_join_gui(player)
	local frame = player.gui.center.add{type="frame", name="labyrinth_join", caption={"join.join"}}.add{type="table", name="labyrinth_join_table", colspan=1}

	local settings = frame.add{type="table", name="labyrinth_settings", colspan=2}
	settings.add{type="label", name="labyrinth_join_phases", caption={"join.phases"}}
	settings.add{type="label", name="labyrinth_join_phases2", caption=global.total_phases}
	settings.add{type="label", name="labyrinth_join_delay", caption={"join.delay"}}
	settings.add{type="label", name="labyrinth_join_delay2", caption=global.delay}
	settings.add{type="label", name="labyrinth_join_mazesize", caption={"join.maze"}}
	settings.add{type="label", name="labyrinth_join_mazesize2", caption=global.mazesize}
	settings.add{type="label", name="labyrinth_join_cellsize", caption={"join.cells"}}
	settings.add{type="label", name="labyrinth_join_cellsize2", caption=global.cellsize}
	settings.add{type="label", name="labyrinth_join_capture", caption={"join.capture"}}
	settings.add{type="label", name="labyrinth_join_capture2", caption=global.capture_time}
	settings.add{type="label", name="labyrinth_join_biters", caption={"join.biters"}}
	if global.biters then settings.add{type="label", name="labyrinth_join_biters2", caption="true"}
	else settings.add{type="label", name="labyrinth_join_biters2", caption="false"} end
	settings.add{type="label", name="labyrinth_join_win", caption={"join.win"}}
	settings.add{type="label", name="labyrinth_join_win2", caption={"conditions." .. global.win_condition}}
	if global.win_condition == "Domination" then
		settings.add{type="label", name="labyrinth_join_win3", caption={"join.dom"}}
		settings.add{type="label", name="labyrinth_join_win4", caption=math.ceil((global.mazesize * global.mazesize) * (global.domination_limit / 100))}
	elseif global.win_condition == "Elimination" then
		settings.add{type="label", name="labyrinth_join_win3", caption={"join.elim"}}
		settings.add{type="label", name="labyrinth_join_win4", caption=global.elimination_limit}
	end

	local players = frame.add{type="table", name="labyrinth_join_players", colspan=3}
	players.style.horizontal_spacing = 25
	players.add{type="label", name="labyrinth_join_players_current", caption={"join.online"}}
	players.add{type="label", name="labyrinth_join_players_current_green", caption=#game.forces["Green"].connected_players}.style.font_color = colors.green
	players.add{type="label", name="labyrinth_join_players_current_blue", caption=#game.forces["Blue"].connected_players}.style.font_color = colors.blue
	players.add{type="label", name="labyrinth_join_players_total", caption={"join.total"}}
	players.add{type="label", name="labyrinth_join_players_total_green", caption=#game.forces["Green"].players}.style.font_color = colors.green
	players.add{type="label", name="labyrinth_join_players_total_blue", caption=#game.forces["Blue"].players}.style.font_color = colors.blue


	local buttons = frame.add{type="table", name="labyrinth_join_buttons", colspan=3}
	buttons.add{type="button", name="labyrinth_join_green", caption={"buttons.join", {"teams.green"}}}.style.font_color = colors.green
	buttons.add{type="button", name="labyrinth_join_blue", caption={"buttons.join", {"teams.blue"}}}.style.font_color = colors.blue
	--TODO: Spectate Mode
	--buttons.add{type="button", name="labyrinth_join_spectate", caption="Join Spectators"}.style.font_color = {r=105/255,g=105/255,b=105/255}
end

--Create the Map and Status buttons on each player's GUI and make the join button visible
Event.register(game_started, function(event) --luacheck: ignore event
	for _,p in pairs(game.players) do
		p.gui.top.add{type="button", name="open_labyrinth_map", caption={"buttons.map"}}
		if p.gui.center.labyrinth_welcome then
			p.gui.center.labyrinth_welcome.labyrinth_welcome_table.labyrinth_welcome_close.style.visible = true
		end
		create_status_gui(p)
	end
end)

--Change the color of cells in open maps and update tooltips
Event.register(cell_captured, function(event)
	local color = event.team
	if color == "Green" then color = "green"
	elseif color == "Blue" then color = "blue"
	end
	for _,p in pairs(game.players) do
		if p.gui.center.labyrinth_map_frame then
			if p.gui.center.labyrinth_map_frame.labyrinth_wrapper.labyrinth_map_selection.labyrinth_map_control.state then
				p.gui.center.labyrinth_map_frame.labyrinth_wrapper.labyrinth_map["labyrinth_map_" .. event.cell.x .. "_" .. event.cell.y].sprite = "virtual-signal/signal-" .. color
			end
			p.gui.center.labyrinth_map_frame.labyrinth_wrapper.labyrinth_map["labyrinth_map_" .. event.cell.x .. "_" .. event.cell.y].tooltip = build_tooltip(event.cell.x, event.cell.y)
		end
	end
end)

Event.register(defines.events.on_gui_click, function(event)
	if not event.element or not event.element.valid then return end
	local name = event.element.name
	local player = game.players[event.player_index]
	--Begin the round
	if name == "labyrinth_complete" then
		--TODO: Add error checking
		local ctable = event.element.parent.labyrinth_table
		global.total_phases = tonumber(ctable.labyrinth_phases.text)
		global.mazesize = tonumber(ctable.labyrinth_mazesize.text)
		global.cellsize = tonumber(ctable.labyrinth_cellsize.text)
		global.delay = tonumber(ctable.labyrinth_delay.text)
		global.capture_time = tonumber(ctable.labyrinth_capture.text)
		global.biters = ctable.labyrinth_biters.state
		if ctable.labyrinth_win.selected_index == 1 then
			global.win_condition = "Domination"
		elseif ctable.labyrinth_win.selected_index == 2 then
			global.win_condition = "Elimination"
		end
		global.domination_limit = tonumber(ctable.labyrinth_domination.text)
		global.elimination_limit = tonumber(ctable.labyrinth_elimination.text)
		--luacheck: globals begin_round
		begin_round()

	--Begin map and status buttons
	elseif name == "open_labyrinth_map" then
		if player.gui.center.labyrinth_map_frame then
			player.gui.center.labyrinth_map_frame.destroy()
		else
			create_map(player)
		end
	elseif name == "close_labyrinth_map" then
		player.gui.center.labyrinth_map_frame.destroy()
	elseif name == "labyrinth_map_control" then
		event.element.state = true
		event.element.parent.labyrinth_map_bonuses.state = false
		redraw_map(player, "control")
	elseif name == "labyrinth_map_bonuses" then
		event.element.state = true
		event.element.parent.labyrinth_map_control.state = false
		redraw_map(player, "bonuses")
	elseif name == "labyrinth_status_open" then
		event.element.style.visible = false
		event.element.parent.labyrinth_status_toggle_frame.style.visible = true
	elseif name == "labyrinth_status_close" then
		event.element.parent.parent.style.visible = false
		event.element.parent.parent.parent.labyrinth_status_open.style.visible = true

	--Begin debug buttons
	elseif name == "labyrinth_debug_menu" then
		create_debug_menu(player)
	elseif name == "debug_begin_round" then
		begin_round()
	elseif name == "debug_open_setup" then
		player.gui.center.labyrinth_debug_menu.destroy()
		create_setup_gui(player)
	elseif name == "debug_open_join" then
		if not global.started then
			player.print("There must be a game running before the join GUI can be opened!")
			return
		end
		player.gui.center.labyrinth_debug_menu.destroy()
		create_join_gui(player)
	elseif name == "debug_teleport" then
		if not game.surfaces.Labyrinth then
			player.print("The Labyrinth surface does not exist! Start a new round first.")
			return
		end
		player.teleport({0,0}, "Labyrinth")
	elseif name == "debug_green" then
		player.force = "Green"
	elseif name == "debug_blue" then
		player.force = "Blue"
	elseif name == "debug_labyrinth" then
		player.force = "Labyrinth"
	elseif name == "debug_research" then
		player.force.research_all_technologies()
	elseif name == "debug_open_gates" then
		global.open_all_gates = true
		for _,arrays in pairs(global.control_behaviors) do
			for _,behavior in pairs(arrays) do
				behavior.circuit_condition = {condition = {comparator="=", first_signal={type="virtual",name="signal-O"}, second_signal={type="virtual",name="signal-O"}}, fulfilled = false}
			end
		end
	elseif name == "debug_resume_gates" then
		global.resume_gates = true
		for _,arrays in pairs(global.control_behaviors) do
			for _,behavior in pairs(arrays) do
				behavior.circuit_condition = {condition = {comparator="≠", first_signal={type="virtual",name="signal-O"}, second_signal={type="virtual",name="signal-O"}}, fulfilled = false}
			end
		end
	elseif name == "debug_activate_bonuses" then
		global.activate_bonuses = true
		global.deactivate_bonuses = false
	elseif name == "debug_deactivate_bonuses" then
		global.activate_bonuses = false
		global.deactivate_bonuses = true
	elseif name == "debug_resume_bonuses" then
		global.activate_bonuses = false
		global.deactivate_bonuses = false
	elseif name == "close_labyrinth_debug" then
		player.gui.center.labyrinth_debug_menu.destroy()

	--Begin player join GUIs
	elseif name == "labyrinth_welcome_close" then
		player.gui.center.labyrinth_welcome.destroy()
		create_join_gui(player)
	elseif name == "labyrinth_join_green" then
		player.gui.center.labyrinth_join.destroy()
		player.force = "Green"
		player.teleport(game.surfaces.Labyrinth.find_non_colliding_position("player", game.forces["Green"].get_spawn_position("Labyrinth"),0,1), "Labyrinth")
		for _,p in pairs(game.players) do
			if p.gui.center.labyrinth_join then
				p.gui.center.labyrinth_join.labyrinth_join_table.labyrinth_join_players.labyrinth_join_players_current_green.caption = #game.forces["Green"].connected_players
				p.gui.center.labyrinth_join.labyrinth_join_table.labyrinth_join_players.labyrinth_join_players_total_green.caption = #game.forces["Green"].players
			end
		end
		player.color = colors.green
		player.insert{name="iron-plate", count=8}
		player.insert{name="pistol", count=1}
		player.insert{name="firearm-magazine", count=10}
		player.insert{name="burner-mining-drill", count = 1}
		player.insert{name="stone-furnace", count = 1}
		player.print{"announcements.join1", {"teams.green"}, "bottom"}
		player.print{"announcements.join2"}
	elseif name == "labyrinth_join_blue" then
		player.gui.center.labyrinth_join.destroy()
		player.force = "Blue"
		player.teleport(game.surfaces.Labyrinth.find_non_colliding_position("player", game.forces["Blue"].get_spawn_position("Labyrinth"),0,1), "Labyrinth")
		for _,p in pairs(game.players) do
			if p.gui.center.labyrinth_join then
				p.gui.center.labyrinth_join.labyrinth_join_table.labyrinth_join_players.labyrinth_join_players_current_blue.caption = #game.forces["Blue"].connected_players
				p.gui.center.labyrinth_join.labyrinth_join_table.labyrinth_join_players.labyrinth_join_players_total_blue.caption = #game.forces["Blue"].players
			end
		end
		player.color = colors.blue
		player.insert{name="iron-plate", count=8}
		player.insert{name="pistol", count=1}
		player.insert{name="firearm-magazine", count=10}
		player.insert{name="burner-mining-drill", count = 1}
		player.insert{name="stone-furnace", count = 1}
		player.print{"announcements.join1", {"teams.blue"}, "top"}
		player.print{"announcements.join2"}
	end
end)
